public class Reverse extends ColoredCard{

    public Reverse(String color){
        super(color);
    }

    public boolean canPlay(UnoCard c){
        if(this.cardColor == c.getColor()){
            return true;
        }
        else if(this.getNum() == c.getNum()){
            return true;
        }
        return false;
    }
    
    public int getNum(){
        return 97;
    }
}
