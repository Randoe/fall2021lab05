public class Plus2 extends ColoredCard{
    private int addNum = 4;

    public Plus2(String color){
        super(color);
    }

    public boolean canPlay(UnoCard c){
        if(this.cardColor == c.getColor()){
            return true;
        }
        else if(this.getNum() == c.getNum()){
            return true;
        }
        return false;
    }

    public int getAddNum(){
        return this.addNum;
    }

    public int getNum(){
        return 98;
    }
}
