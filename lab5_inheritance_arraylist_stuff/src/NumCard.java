public class NumCard extends ColoredCard{
    private int num;

    public NumCard(int num, String color){
        super(color);
        this.num = num;
    }
    public int getNum(){
        return this.num;
    }

    public boolean canPlay(UnoCard c){
        if(c.getColor() == this.cardColor){
            return true;
        }
        else if(this.num == ((NumCard) c).getNum()){
            return true;
        }
        return false;
    }
}
