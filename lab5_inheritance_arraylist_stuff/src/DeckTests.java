import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;

public class DeckTests {
    @Test
    public void makeDeckTest(){
        int expected = 112;
        Deck d = new Deck();
        assertEquals(expected, d.getSize());
    }
    @Test
    public void testFirstCard(){
        Deck d = new Deck();
        assertTrue(d.getnthCard(0) instanceof ChangeC);
    }
    @Test
    public void testLastCard(){
        Deck d = new Deck();
        assertTrue(d.getnthCard(111) instanceof NumCard);
    }
    @Test
    public void testMidCard(){
        Deck d = new Deck();
        assertTrue(d.getnthCard(56) instanceof NumCard);
    }
    @Test
    public void testPowerPlus2Card(){
        Deck d = new Deck();
        assertTrue(d.getnthCard(8) instanceof Plus2);
    }
    @Test
    public void testPowerPlus4Card(){
        Deck d = new Deck();
        assertTrue(d.getnthCard(1) instanceof Plus4);
    }
    @Test
    public void testPowerReverseCard(){
        Deck d = new Deck();
        assertTrue(d.getnthCard(9) instanceof Reverse);
    }
    @Test
    public void testPowerSkipCard(){
        Deck d = new Deck();
        assertTrue(d.getnthCard(10) instanceof Skip);
    }
    @Test
    public void testAddToDeck(){
        Deck d = new Deck();
        NumCard c = new NumCard(9, "red");
        d.addToDeck(c);
        assertEquals(113, d.getSize());
    }
    @Test
    public void testDraw(){
        Deck d = new Deck();
        d.draw();
        d.draw();
        assertEquals(110, d.getSize());
    }
    @Test
    public void testCanPlayPlus4Card(){
        Deck d = new Deck();
        NumCard c = new NumCard(5, "green");
        assertTrue(d.getnthCard(0).canPlay(c));
    }
    @Test
    public void testCanPlayWildCard(){
        Deck d = new Deck();
        NumCard c = new NumCard(8, "blue");
        assertTrue(d.getnthCard(3).canPlay(c));
    }
    @Test
    public void testCanPlayNumCard(){
        Deck d = new Deck();
        NumCard c = new NumCard(0, "yellow");
        assertTrue(d.getnthCard(11).canPlay(c));
    }
    @Test
    public void testCantPlayNumCard(){
        Deck d = new Deck();
        NumCard c = new NumCard(0, "yellow");
        assertFalse(d.getnthCard(12).canPlay(c));
    }
    @Test
    public void testCantPlayNumSameNum(){
        Deck d = new Deck();
        NumCard c = new NumCard(6, "green");
        assertFalse(d.getnthCard(12).canPlay(c));
    }
    @Test
    public void testCanPlayNumSameNum(){
        Deck d = new Deck();
        NumCard c = new NumCard(6, "blue");
        assertTrue(d.getnthCard(17).canPlay(c));
    }
    @Test
    public void testCanPlayPlus2Card(){
        Deck d = new Deck();
        NumCard c = new NumCard(7, "green");
        assertTrue(d.getnthCard(73).canPlay(c));
    }
    @Test
    public void testCantPlayPlus2Card(){
        Deck d = new Deck();
        NumCard c = new NumCard(7, "red");
        assertFalse(d.getnthCard(73).canPlay(c));
    }
    @Test
    public void testCanPlaySkipCard(){
        Deck d = new Deck();
        Skip c = new Skip("blue");
        assertTrue(d.getnthCard(36).canPlay(c));
    }
    @Test
    public void testCanPlaySkipCardSameSymbol(){
        Deck d = new Deck();
        Skip c = new Skip("red");
        assertTrue(d.getnthCard(36).canPlay(c));
    }
    @Test
    public void testCantPlaySkipCard(){
        Deck d = new Deck();
        Reverse c = new Reverse("red");
        assertFalse(d.getnthCard(36).canPlay(c));
    }
    @Test
    public void testCanPlayReverseCardSameSymbol(){
        Deck d = new Deck();
        Reverse c = new Reverse("yellow");
        assertTrue(d.getnthCard(61).canPlay(c));
    }
    @Test
    public void testCanPlayReverseCard(){
        Deck d = new Deck();
        Skip c = new Skip("green");
        assertTrue(d.getnthCard(61).canPlay(c));
    }
    @Test
    public void testCantPlayReverseCard(){
        Deck d = new Deck();
        Skip c = new Skip("yellow");
        assertFalse(d.getnthCard(74).canPlay(c));
    }
}
