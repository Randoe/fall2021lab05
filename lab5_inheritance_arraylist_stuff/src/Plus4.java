public class Plus4 extends WildCard{
    private int addNum = 4;

    public Plus4(String color){
        super(color);
    }

    public String getColor(){
        return this.changeColor;
    }

    public boolean canPlay(UnoCard c){
        return true;
    }

    public int getAddNum(){
        return this.addNum;
    }

    public int getNum(){
        return 99;
    }
}
