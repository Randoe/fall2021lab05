import java.util.ArrayList;
public class Deck{
    ArrayList<UnoCard> cards;
    
    public Deck(){
        this.cards = new ArrayList<UnoCard>();
        for(int i=0; i<4; i++){
            ChangeC x = new ChangeC("black");
            Plus4 y = new Plus4("black");
            this.cards.add(x);
            this.cards.add(y);
        }
        for(int k=0; k<4; k++){
            for(int l=0; l<2; l++){
                Plus2 p2;
                Reverse r;
                Skip s;
                if(k == 0){
                    p2 = new Plus2("red");
                    r = new Reverse("red");
                    s = new Skip("red");
                }
                else if(k == 1){
                    p2 = new Plus2("blue");
                    r = new Reverse("blue");
                    s = new Skip("blue");
                }
                else if(k == 2){
                    p2 = new Plus2("green");
                    r = new Reverse("green");
                    s = new Skip("green");
                }
                else{
                    p2 = new Plus2("yellow");
                    r = new Reverse("yellow");
                    s = new Skip("yellow");
                }
                this.cards.add(p2);
                this.cards.add(r);
                this.cards.add(s);

                for(int m=0; m<10; m++){
                    NumCard z;
                    if(k == 0){
                        z = new NumCard(m, "red");
                    }
                    else if(k == 1){
                        z = new NumCard(m, "blue");
                    }
                    else if(k == 2){
                        z = new NumCard(m, "green");
                    }
                    else{
                        z = new NumCard(m, "yellow");
                    }
                    this.cards.add(z);
                }
            }
        }
    }

    public void addToDeck(UnoCard c){
        this.cards.add(c);
    }

    public UnoCard draw(){
       UnoCard c = this.cards.get(0);
       this.cards.remove(0);
       return c;
    }

    public int getSize(){
        return this.cards.size();
    }

    public UnoCard getnthCard(int i){
        return this.cards.get(i);
    }
}
