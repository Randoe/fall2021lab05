public abstract class ColoredCard implements UnoCard{
    protected String cardColor;

    public String getColor(){
        return this.cardColor;
    }

    public ColoredCard(String color){
        this.cardColor = color;
    }
}


