public class ChangeC extends WildCard{
    
    public ChangeC(String color){
        super(color);
    }

    public String getColor(){
        return this.changeColor;
    }

    public boolean canPlay(UnoCard c){
        return true;
    }

    public int getNum(){
        return 100;
    }
}
